
mod secrets;
use secrets::Secrets;


pub mod events;

// The environment variable from which to read the token 
pub const TOKEN_ENV_VARIABLE: &str = "CRUSTY_TOKEN";

// Crusty Types
/// Crusty's fuckys wuckys
pub type Error = Box<dyn std::error::Error + Send + Sync>;
/// Crusty's context, which allows the user to grab the serenity context + data struct
pub type Context<'a> = poise::Context<'a, Data, Error>;
/// A wrapped around the Poise command context, for ease of use.
pub type Command = poise::Command<Data, Error>;




pub struct Data {
    pub secrets: Secrets,
}


pub async fn initialise_data() -> Data {
    let secrets = match Secrets::get("config/secrets.toml".to_string()) {
        Ok(secrets) => secrets,
        Err(err) => panic!("Error reading the secrets configuration: {err}"),
    };

    Data { 
        secrets
     }
}