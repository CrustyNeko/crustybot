
use crate::{Data,Error};


/// **Crusty error handler**
/// 
/// Called by the poise framework on each errors, we use it to handle the errors we care about,  
pub async fn on_error(error: poise::FrameworkError<'_, Data, Error>) {
    
}