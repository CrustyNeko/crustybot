pub mod on_error;


use crate::{Data, Error};
use poise::serenity_prelude::Context;


/// **Crusty's own little event listener
/// 
/// Absolutely not implemented
/// TODO
pub async fn event_listener(
    ctx: &Context,
    event: &poise::Event<'_>,
    framework: poise::FrameworkContext<'_, Data, Error>,
    user_data: &Data
) -> Result<(), Error> {
    Ok(())
}