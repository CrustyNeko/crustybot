use serde::{Deserialize, Serialize};
use toml;

use std::fs::File;
use std::io::Read;

/// Structure for `secrets.toml`
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Secrets {
    pub discord_token: Option<String>,
    pub bot_owners: Option<Vec<u64>>
}

impl Secrets {
    /// Reads secrets from secrets configuration file
    pub fn get(path: String) -> Result<Secrets, String> {
        let mut file = match File::open(path) {
            Ok(file) => file,
            Err(err) => return Err(format!("Could not open file: {}", err)),
        };

        let mut contents = String::new();
        match file.read_to_string(&mut contents) {
            Ok(_) => (),
            Err(err) => return Err(format!("Could not read file: {}", err)),
        };

        match toml::from_str(&contents) {
            Ok(secrets) => Ok(secrets),
            Err(err) => Err(format!("Could not deserialize configuration file: {}", err)),
        }
    }
}