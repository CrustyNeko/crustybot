


use crustycore::Command;


mod commands;

pub fn commands() -> Vec<Command> {
    commands::commands().into_iter().collect()
}
