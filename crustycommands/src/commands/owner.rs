use crustycore::{Context, Error};
use poise::samples::register_application_commands_buttons;

use crate::commands::owner::register::register;

mod register;

/// Owner only commands. The bot owner can only execute these, unless you are super, mega special
#[poise::command(
    owners_only,
    prefix_command,
    slash_command,
    category = "Owner",
    subcommands("register")
)]
pub async fn owner(ctx: Context<'_>) -> Result<(), Error> {
    register_application_commands_buttons(ctx).await?;
    Ok(())
}