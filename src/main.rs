
use crustycore::{
    // Data, 
    // Error, 
    initialise_data,
    events::{event_listener, on_error::on_error}, 
    TOKEN_ENV_VARIABLE, 
};
use crustycommands::commands;

use poise::{
    serenity_prelude::{GatewayIntents, UserId},
    FrameworkOptions
};
use std::collections::HashSet;

#[tokio::main]
async fn main() {

    let data = initialise_data().await;
    // Attempt to get a token from `secrets.toml`. If it does not exist, try to get it from the environment variable defined by [BOT_TOKEN].
    // If that ALSO does not exist, insult the user for being incompetent.
    let token = match data.secrets.discord_token.clone() {
        Some(t) => t,
        None => match std::env::var(TOKEN_ENV_VARIABLE) {
            Ok(environment_token) => environment_token,
            Err(err) => panic!("Congrats, you didn't set either {TOKEN_ENV_VARIABLE} or include the token in the config file. Terminating on your sheer stupidity.\n{err}")
        }
    };
        // Framework Options
        let mut framework_options = FrameworkOptions {
            commands: commands(),
            on_error: |error| Box::pin(on_error(error)),
            event_handler: |ctx, event, framework, user_data| {
                Box::pin(async move {
                    event_listener(ctx, event, framework, user_data).await?;
                    Ok(())
                })
            },
            ..Default::default()
        };
    
        // If owners are configured, override the framework options with said owner
        if let Some(owners) = &data.secrets.bot_owners {
            let owners_map: HashSet<UserId> = owners.iter().map(|user_id| UserId(*user_id)).collect();
            framework_options.owners = owners_map;
        };
    match poise::Framework::builder()
        .options(framework_options)
        .setup(|_, _, _| Box::pin(async { Ok(data) }))
        .token(token)
        .intents(
            GatewayIntents::non_privileged()
                | GatewayIntents::MESSAGE_CONTENT
                | GatewayIntents::GUILD_MEMBERS
                | GatewayIntents::GUILD_PRESENCES
        )
        .run()
        .await
    {
        Ok(_) => println!("Crusty has started!"),
        Err(err) => panic!("Crusty crashed :c\n {err}")
    };
}
